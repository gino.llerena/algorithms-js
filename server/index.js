/**
 * Created by GINO on 07/10/2015.
 */

//library components
var  debug = require('debug')('app:' + process.pid);
var bodyParser = require('body-parser');
var multer = require('multer');
var fs = require("fs");
var jwt = require('express-jwt');
var unless = require('express-unless');
var onFinished = require('on-finished');

//Application modules
var ServerConstants = require('./ServerConstants');
var Config = ServerConstants.Config;
var NotFoundError = require('./errors/NotFoundError');


debug("Starting Application");


debug("Initializing express");
var express = require('express'), app = express();

debug("Attaching plugins");
app.use(require('morgan')('dev'));
app.use(bodyParser.json({limit: '50mb'}));
//app.use(multer({inMemory:true}));
//app.use(bodyParser.urlencoded());
app.use(require('compression')());
//app.use(require('response-time')()); //don't know if we really need this

app.use(function (req, res, next) {
  onFinished(res, function (err) {
    debug('[%s] finished request', req.connection.remoteAddress); //we can do log something when the request is finished
  });
  next();
});

app.use('/', express.static(Config.PUBLIC_DIR));

var jwtCheck = jwt({
  secret: ServerConstants.JWT.secret
});
jwtCheck.unless = unless;
app.use(jwtCheck.unless({
  path: ['/api/login', '/api/user', '/api/users', '/api/test', '/api/confirm', '/api/newpass','/api/clockInOut', '/api/item', '/api/items', '/api/servicesItems', '/api/contacts']
}));
app.use('/api', require('./routes/ApiRouter'));

app.get("*", function (req, res, next) {
  next(new NotFoundError('404'));
});


// final middleware to catch errors and display something to the user.

app.use(function (err, req, res, next) {
  var errorType = typeof err;
  var code = 500;
  var errorMessage = errorType === 'string' || !err.message? 'Internal Server Error': err.message;
  var msg = {message: errorMessage};
  console.error(err.stack);
  switch (err.name) {
    case "UnauthorizedError":
      code = err.status;
      msg = undefined;
      break;
    case "BadRequest":
    case "UnauthorizedAccessError":
    case "NotFoundError":
      code = err.status;
      msg = err.inner;
      break;
    default:
      break;
  }
  res.status(code).json(msg);
});


app.listen(Config.PORT, function () {
  debug("Server started on PORT = %s , in mode %s", Config.PORT, app.get('env'));
});