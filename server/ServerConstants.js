/**
 * Created by GINO on 07/10/2015.
 */

var path = require("path");

module.exports = {

  Config: {
    PORT: process.env.PORT || 3000,
    PUBLIC_DIR: path.resolve(__dirname+"/../build/"),

    MONGOOSE_URI: (function(){
      var environment = process.env.ENVIRONMENT;
      switch (environment){
        case 'prod':
          return process.env.MONGOOSE_URI_PROD;
          break;
        case 'stg':
          return process.env.MONGOOSE_URI_STG;
          break;
        default :
          return 'localhost/xxxx';
          break;
      }
    })(),

    MYSQL_CONFIG: ({
      connectionLimit: 100,
      user: 'admin',
      password: 'admin123',
      database: 'worthy',
      debug: false,
      host: function(){
        var enviroment = process.env.ENVIROMENT;
        switch (enviroment){
          case 'prod':
            return process.env.MYSQL_URI_PROD;
            break;
          case 'stg':
            return process.env.MYSQL_URI_PROD;
            break;
          default:
            return 'localhost';
            break;
        }
      }
    })
  },
  GoogleConfig:{
    auth: {
      user: 'xxxxxx',
      pass: 'abc123--'
    },
    defaultFromAddress: ' <xxxxxx@gmail.com>'
  },

  JWT: {
    secret: 'xxxx'
  }
};