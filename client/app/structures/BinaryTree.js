/**
 * Created by ginollerena on 6/30/18.
 */

export default class BinaryTree {

  constructor() {
    this.root = {};
    this.branch = []
  }

  createNode(value){
    return {value : value, left : {}, right : {}}
  }

  _insertNode(node, value){
    if(!node.value){
      let newNode =  this.createNode(value);
      node = Object.assign(node, newNode);

    } else if (node.value > value){
      this._insertNode(node.left, value)
    } else{
      this._insertNode(node.right, value)
    }
  }

  _height(node) {
    /* base case tree is empty */
    if(!node.value){
      return 0;
    }
    /* If tree is not empty then height = 1 + max of left height and right heights */
    return (1 + Math.max(this.height(node.left), this.height(node.right)));
  }


  _diameter(node){
    /* base case where tree is empty */
    if (!node.value)
      return 0;

    /* get the height of left and right sub-trees */
    const lheight = this._height(node.left);
    const rheight = this._height(node.right);

    /* get the diameter of left and right sub-trees */
    const ldiameter = this._diameter(node.left);
    const rdiameter = this._diameter(node.right);

    /* Return max of following three
     1) Diameter of left subtree
     2) Diameter of right subtree
     3) Height of left subtree + height of right subtree + 1 */
    return Math.max(lheight + rheight + 1, Math.max(ldiameter, rdiameter));
  }

  _preOrder(node){
    if(!node.value)
      return;

    console.log(node.value);

    this._preOrder(node.left);
    this._preOrder(node.right);
  }

  _getBranchUtil(node,  branch, fncConditionUtil) {

    if(!node.value)
      return branch;

    let myBranchL = this._getBranchUtil(node.left, [...branch].concat([node.value]), fncConditionUtil);
    let myBranchR = this._getBranchUtil(node.right, [...branch].concat([node.value]), fncConditionUtil);

    let sumA = myBranchL && myBranchL.length > 0 ? fncConditionUtil(myBranchL) : 0;
    let sumB = myBranchR && myBranchR.length > 0 ? fncConditionUtil(myBranchR) : 0;

    if(sumA > sumB)
      return myBranchL;
    else
      return myBranchR;
  }

  getBranch(fnc){
    if(!fnc){
      fnc = (l) => {return l.reduce((acc,n)=> {acc+=n; return acc}, 0)};
    }
    return this._getBranchUtil(this.root, this.branch, fnc);
  }

  getPreOrder(){
    this._preOrder(this.root);
  }

  getHeight(){
    return this.height(this.root);
  }

  getDiameter(){
    return this._diameter(this.root);
  }

  doNodeInsertion(value){
    this._insertNode(this.root, value);
  }




}