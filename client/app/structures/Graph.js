/**
 * Created by ginollerena on 6/26/18.
 */

import size from 'lodash/size'
import bellmanFor from '../algorithms/graphs/bellmanFor'
import floyd_warshall from '../algorithms/graphs/floyd-warshall'
import dijkstra from '../algorithms/graphs/dijkstra'

const Infinite = Number.MAX_SAFE_INTEGER;

class Graph {

  constructor(data) {
    this.graph = {};
    this.edges = [];
    if (data) {
      this.edges = data;
      this.graph = Graph.getGraph(data);
    }
  }

  getGraph() {
    return this.graph;
  }

  getAllEdges(){
    return this.edges;
  }

  _getAllNodes(){
    const keys = Reflect.ownKeys(this.graph);

    const nodes = keys.reduce((nodes, key) =>{
      if(!nodes[key])
        nodes[key] = key;

      const edges = Reflect.ownKeys(this.graph[key]);
      edges.forEach((key)=>{
        if(!nodes[key])
          nodes[key] = key;
      })
      return nodes;

    }, {});

    const allNodes = Reflect.ownKeys(nodes);

    return allNodes;
  }

  initializeMatrixObject(keys, value){

    return keys.reduce((acc, key)=> {
      acc[key] = key.map((k)=> ({[k]:0}));
      return acc;
    },{})
  }

  initializeMatrix(n){
    return Array.from(new Array(n), () => Array.from(new Array(n), ()=> 0));
  }

  getAllNodes(){
    const keys = Reflect.ownKeys(this.graph);
    const allKeys = keys.reduce((acc,key)=>{
      acc[key] = key;
      const neighbors = Reflect.ownKeys(this.graph[key]);
      neighbors.forEach((neighbor)=>{
        if(!acc[neighbor])
          acc[neighbor] = neighbor;
      })
      return acc;
    },{})

   const sortedKeys =  Reflect.ownKeys(allKeys).sort((a,b)=> a - b);
   return sortedKeys;
  }


  getAdjMatrix(graph){

    const keys = this.getAllNodes();
    const adjMatrix = this.initializeMatrix(keys.length);

    keys.forEach((key) =>{
        const i = keys.findIndex(vertex => vertex.toString() === key.toString());
        const edges = graph[key] ? Reflect.ownKeys(graph[key]) : [];
        edges.forEach((edge)=>{
          const j = keys.findIndex(link => link.toString() === edge.toString());
          adjMatrix[i][j] =  graph[key] && graph[key][edge] ? graph[key][edge] : 0;
        });
    });

    return adjMatrix;
  }

  getDijkstra(startNode, endNode){
    return dijkstra(this.graph, startNode, endNode);
  }

  getFloydMarshall() {
    const keys = this.getAllNodes();
    const pathMatrix = this.initializeMatrix(keys.length);
    const adjMatrix = this.getAdjMatrix(this.graph);

    floyd_warshall(adjMatrix, pathMatrix, keys.length);

    return {pathMatrix, adjMatrix}
  }

  static getGraph(data) {

    const graph = data.reduce((acc, edge, i)=> {

      const a = edge[0];
      const b = edge[1];

      const weight = edge[2];

      if (i === 0) {
        acc[a] = {[b]: weight};
      }
      else {
        if (!acc[a]) {
          acc[a] = {[b]: weight};
        } else {
          acc[a] = {...acc[a], [b]: weight}
        }
      }
      return acc;
    }, {})

    return graph;
  }

  getBellmanFor(s){
    return bellmanFor(this.getAllNodes(), this.getAllEdges(), s)
  }

}

export default Graph;