
import React from 'react'
import ReactDOM from 'react-dom'

import Graph from './structures/Graph'
import BinaryTree from './structures/BinaryTree'


function Sum(l) {return l.reduce((acc,n)=> {acc+=n; return acc}, 0)}

function SumDiscardDuplicates(l){

  const x = l.reduce((acc,n)=> {if(!acc[n]){acc[n]=1;} else{acc[n]++;} return acc}, {});
  const s = Object.values(x).filter(n=> n > 1)

  if(s.length >= 1)
    return 0;

  return Sum(l);
}


const list = [
  [1, 2, 3],
  [2, 1, 3],
  [2, 3, 1],
  [3, 2, 1],
  [1, 4, 4],
  [4, 1, 4],
  [4, 6, 7],
  [6, 4, 7],
  [7, 5, 2],
  [5, 7, 2],
  [3, 5, 1],
  [5, 3, 1],
  [4, 5, 5],
  [5, 4, 5]
]


const list1 = [
  [0, 1, -1],
  [0, 2, 4],
  [1, 2, 3],
  [1, 3, 2],
  [1, 4, 2],
  [3, 2, 5],
  [3, 1, 1],
  [4, 3, -3]

];

const myGraph1 = new Graph(list1);
console.log(myGraph1.graph);
console.log('bellmanFor', myGraph1.getBellmanFor(0));
console.log('floydMarshall', myGraph1.getFloydMarshall(0));
console.log('Dijkstra', myGraph1.getDijkstra(0,4));


let myTree = new BinaryTree();

myTree.doNodeInsertion(5);
myTree.doNodeInsertion(10);
myTree.doNodeInsertion(15);
myTree.doNodeInsertion(5);
myTree.doNodeInsertion(2);
myTree.doNodeInsertion(3);
myTree.doNodeInsertion(12);
myTree.doNodeInsertion(17);
myTree.doNodeInsertion(11);

console.log('Branch',myTree.getBranch(Sum));


/*
const myGraph = new Graph(list);
console.log('graph', myGraph.graph);
console.log('allnodes', myGraph.getAllNodes());
console.log('bellmanFor', myGraph.bellmanFor(0));
const graph = Graph.getGraph(list);

myGraph.getAdjMatrix(graph);
myGraph.getFloydMarshall(graph);



console.log(graph);

console.log(Graph.dijkstra(graph,'1','7'));
*/

ReactDOM.render(<div>Graph</div>, document.getElementById('app-container'));