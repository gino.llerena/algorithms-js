/**
 * Created by ginollerena on 6/29/18.
 */

export default function bellmanFor(nodes, edges, s){
  let distances = {};
  let parents = {}
  nodes.forEach(node => {
    distances[node] = Infinity;
    parents[node] = null;
  });
  distances[s] = 0;

  for (let iteration = 0; iteration < (nodes.length - 1); iteration += 1) {
    edges.forEach(edge =>{
      const [from, to, weight] = edge;
      if (distances[from] + weight < distances[to]) {
        distances[to] = distances[from] + weight;
        parents[to] = from;
      }
    });
  };

  edges.forEach(edge =>{
    const [from, to, weight] =  edge;
    if (distances[from] + weight < distances[to]) {
      return undefined;
    }
  });

  return {
    distances,
    parents
  }
}