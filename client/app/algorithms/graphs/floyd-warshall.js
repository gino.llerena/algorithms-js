/**
 * Created by ginollerena on 6/29/18.
 */

export default function floyd_warshall(dist, P, n) {				 //  including array P to record paths

  //  initialise P to all -1s
  for (let k = 0; k < n; ++k)
    for (let i = 0; i < n; ++i)
      for (let j = 0; j < n; ++j) {
        //  check for path from i to k and k to j
        if ((dist[i][k] * dist[k][j] !== 0) && (i !== j)) {
          //  check shorter path from i to k and k to j
          if ((dist[i][k] + dist[k][j] < dist[i][j]) || (dist[i][j] === 0)) {
            dist[i][j] = dist[i][k] + dist[k][j];
            P[i][j] = k+1;
          }
        }
      }
}