/**
 * Created by ginollerena on 6/30/18.
 */

const ENABLE_LOG = false;

function log(text){
  if( ENABLE_LOG)
    console.log(text);

}

function lowestCostNode(costs = [], processed) {
  const keys = Reflect.ownKeys(costs);
  return keys.reduce((lowest, node) => {
    if (lowest === null || costs[node] < costs[lowest]) {
      if (!processed.includes(node)) {
        lowest = node;
      }
    }
    return lowest;
  }, null);
};

// function that returns the minimum cost and path to reach Finish
export default function dijkstra(graph, startNodeName, endNodeName) {

  // track the lowest cost to reach each node
  let costs = {};
  costs[endNodeName] = "Infinity";
  costs = Object.assign(costs, graph[startNodeName]);

  // track paths
  const parents = {endNodeName: null};
  for (let child in graph[startNodeName]) {
    parents[child] = startNodeName;
  }

  // track nodes that have already been processed
  const processed = [];

  let node = lowestCostNode(costs, processed);

  while (node) {
    let cost = costs[node];
    let children = graph[node];
    for (let n in children) {
      if (String(n) === String(startNodeName)) {
        log("WE DON'T GO BACK TO START");
      } else {
        log("StartNodeName: " + startNodeName);
        log("Evaluating cost to node " + n + " (looking from node " + node + ")");
        log("Last Cost: " + costs[n]);
        let newCost = cost + children[n];
        log("New Cost: " + newCost);
        if (!costs[n] || costs[n] > newCost) {
          costs[n] = newCost;
          parents[n] = node;
          log("Updated cost und parents");
        } else {
          log("A shorter path already exists");
        }
      }
    }
    log('node', node);
    processed.push(node);
    node = lowestCostNode(costs, processed);
  }

  let optimalPath = [endNodeName];
  let parent = parents[endNodeName];
  while (parent) {
    optimalPath.push(parent);
    parent = parents[parent];
  }
  optimalPath.reverse();

  const results = {
    distance: costs[endNodeName],
    path: optimalPath
  };

  return results;
};