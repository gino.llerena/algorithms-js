webpackJsonp([0],[
/* 0 */,
/* 1 */
/*!**************************************!*\
  !*** ./node_modules/lodash/_root.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ 41);

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),
/* 2 */,
/* 3 */
/*!*******************************************!*\
  !*** ./node_modules/lodash/_getNative.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var baseIsNative = __webpack_require__(/*! ./_baseIsNative */ 40),
    getValue = __webpack_require__(/*! ./_getValue */ 47);

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

module.exports = getNative;


/***/ }),
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */
/*!********************************************!*\
  !*** ./node_modules/lodash/_baseGetTag.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(/*! ./_Symbol */ 17),
    getRawTag = __webpack_require__(/*! ./_getRawTag */ 43),
    objectToString = __webpack_require__(/*! ./_objectToString */ 44);

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */
/*!*******************************************!*\
  !*** ./node_modules/lodash/isFunction.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ 9),
    isObject = __webpack_require__(/*! ./isObject */ 18);

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;


/***/ }),
/* 17 */
/*!****************************************!*\
  !*** ./node_modules/lodash/_Symbol.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ 1);

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),
/* 18 */
/*!*****************************************!*\
  !*** ./node_modules/lodash/isObject.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),
/* 19 */
/*!******************************************!*\
  !*** ./node_modules/lodash/_toSource.js ***!
  \******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var funcProto = Function.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

module.exports = toSource;


/***/ }),
/* 20 */
/*!***************************!*\
  !*** ./app/main.react.js ***!
  \***************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _react = __webpack_require__(/*! react */ 4);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ 11);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _Graph = __webpack_require__(/*! ./structures/Graph */ 32);

var _Graph2 = _interopRequireDefault(_Graph);

var _BinaryTree = __webpack_require__(/*! ./structures/BinaryTree */ 66);

var _BinaryTree2 = _interopRequireDefault(_BinaryTree);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Sum(l) {
  return l.reduce(function (acc, n) {
    acc += n;return acc;
  }, 0);
}

function SumDiscardDuplicates(l) {

  var x = l.reduce(function (acc, n) {
    if (!acc[n]) {
      acc[n] = 1;
    } else {
      acc[n]++;
    }return acc;
  }, {});
  var s = Object.values(x).filter(function (n) {
    return n > 1;
  });

  if (s.length >= 1) return 0;

  return Sum(l);
}

var list = [[1, 2, 3], [2, 1, 3], [2, 3, 1], [3, 2, 1], [1, 4, 4], [4, 1, 4], [4, 6, 7], [6, 4, 7], [7, 5, 2], [5, 7, 2], [3, 5, 1], [5, 3, 1], [4, 5, 5], [5, 4, 5]];

var list1 = [[0, 1, -1], [0, 2, 4], [1, 2, 3], [1, 3, 2], [1, 4, 2], [3, 2, 5], [3, 1, 1], [4, 3, -3]];

var myGraph1 = new _Graph2.default(list1);
console.log(myGraph1.graph);
console.log('bellmanFor', myGraph1.getBellmanFor(0));
console.log('floydMarshall', myGraph1.getFloydMarshall(0));
console.log('Dijkstra', myGraph1.getDijkstra(0, 4));

var myTree = new _BinaryTree2.default();

myTree.doNodeInsertion(5);
myTree.doNodeInsertion(10);
myTree.doNodeInsertion(15);
myTree.doNodeInsertion(5);
myTree.doNodeInsertion(2);
myTree.doNodeInsertion(3);
myTree.doNodeInsertion(12);
myTree.doNodeInsertion(17);
myTree.doNodeInsertion(11);

console.log('Branch', myTree.getBranch(Sum));

/*
const myGraph = new Graph(list);
console.log('graph', myGraph.graph);
console.log('allnodes', myGraph.getAllNodes());
console.log('bellmanFor', myGraph.bellmanFor(0));
const graph = Graph.getGraph(list);

myGraph.getAdjMatrix(graph);
myGraph.getFloydMarshall(graph);



console.log(graph);

console.log(Graph.dijkstra(graph,'1','7'));
*/

_reactDom2.default.render(_react2.default.createElement(
  'div',
  null,
  'Graph'
), document.getElementById('app-container'));

/***/ }),
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */
/*!*********************************!*\
  !*** ./app/structures/Graph.js ***!
  \*********************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Created by ginollerena on 6/26/18.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */

var _size = __webpack_require__(/*! lodash/size */ 33);

var _size2 = _interopRequireDefault(_size);

var _bellmanFor = __webpack_require__(/*! ../algorithms/graphs/bellmanFor */ 62);

var _bellmanFor2 = _interopRequireDefault(_bellmanFor);

var _floydWarshall = __webpack_require__(/*! ../algorithms/graphs/floyd-warshall */ 63);

var _floydWarshall2 = _interopRequireDefault(_floydWarshall);

var _dijkstra = __webpack_require__(/*! ../algorithms/graphs/dijkstra */ 64);

var _dijkstra2 = _interopRequireDefault(_dijkstra);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Infinite = Number.MAX_SAFE_INTEGER;

var Graph = function () {
  function Graph(data) {
    _classCallCheck(this, Graph);

    this.graph = {};
    this.edges = [];
    if (data) {
      this.edges = data;
      this.graph = Graph.getGraph(data);
    }
  }

  _createClass(Graph, [{
    key: 'getGraph',
    value: function getGraph() {
      return this.graph;
    }
  }, {
    key: 'getAllEdges',
    value: function getAllEdges() {
      return this.edges;
    }
  }, {
    key: '_getAllNodes',
    value: function _getAllNodes() {
      var _this = this;

      var keys = Reflect.ownKeys(this.graph);

      var nodes = keys.reduce(function (nodes, key) {
        if (!nodes[key]) nodes[key] = key;

        var edges = Reflect.ownKeys(_this.graph[key]);
        edges.forEach(function (key) {
          if (!nodes[key]) nodes[key] = key;
        });
        return nodes;
      }, {});

      var allNodes = Reflect.ownKeys(nodes);

      return allNodes;
    }
  }, {
    key: 'initializeMatrixObject',
    value: function initializeMatrixObject(keys, value) {

      return keys.reduce(function (acc, key) {
        acc[key] = key.map(function (k) {
          return _defineProperty({}, k, 0);
        });
        return acc;
      }, {});
    }
  }, {
    key: 'initializeMatrix',
    value: function initializeMatrix(n) {
      return Array.from(new Array(n), function () {
        return Array.from(new Array(n), function () {
          return 0;
        });
      });
    }
  }, {
    key: 'getAllNodes',
    value: function getAllNodes() {
      var _this2 = this;

      var keys = Reflect.ownKeys(this.graph);
      var allKeys = keys.reduce(function (acc, key) {
        acc[key] = key;
        var neighbors = Reflect.ownKeys(_this2.graph[key]);
        neighbors.forEach(function (neighbor) {
          if (!acc[neighbor]) acc[neighbor] = neighbor;
        });
        return acc;
      }, {});

      var sortedKeys = Reflect.ownKeys(allKeys).sort(function (a, b) {
        return a - b;
      });
      return sortedKeys;
    }
  }, {
    key: 'getAdjMatrix',
    value: function getAdjMatrix(graph) {

      var keys = this.getAllNodes();
      var adjMatrix = this.initializeMatrix(keys.length);

      keys.forEach(function (key) {
        var i = keys.findIndex(function (vertex) {
          return vertex.toString() === key.toString();
        });
        var edges = graph[key] ? Reflect.ownKeys(graph[key]) : [];
        edges.forEach(function (edge) {
          var j = keys.findIndex(function (link) {
            return link.toString() === edge.toString();
          });
          adjMatrix[i][j] = graph[key] && graph[key][edge] ? graph[key][edge] : 0;
        });
      });

      return adjMatrix;
    }
  }, {
    key: 'getDijkstra',
    value: function getDijkstra(startNode, endNode) {
      return (0, _dijkstra2.default)(this.graph, startNode, endNode);
    }
  }, {
    key: 'getFloydMarshall',
    value: function getFloydMarshall() {
      var keys = this.getAllNodes();
      var pathMatrix = this.initializeMatrix(keys.length);
      var adjMatrix = this.getAdjMatrix(this.graph);

      (0, _floydWarshall2.default)(adjMatrix, pathMatrix, keys.length);

      return { pathMatrix: pathMatrix, adjMatrix: adjMatrix };
    }
  }, {
    key: 'getBellmanFor',
    value: function getBellmanFor(s) {
      return (0, _bellmanFor2.default)(this.getAllNodes(), this.getAllEdges(), s);
    }
  }], [{
    key: 'getGraph',
    value: function getGraph(data) {

      var graph = data.reduce(function (acc, edge, i) {

        var a = edge[0];
        var b = edge[1];

        var weight = edge[2];

        if (i === 0) {
          acc[a] = _defineProperty({}, b, weight);
        } else {
          if (!acc[a]) {
            acc[a] = _defineProperty({}, b, weight);
          } else {
            acc[a] = _extends({}, acc[a], _defineProperty({}, b, weight));
          }
        }
        return acc;
      }, {});

      return graph;
    }
  }]);

  return Graph;
}();

exports.default = Graph;

/***/ }),
/* 33 */
/*!*************************************!*\
  !*** ./node_modules/lodash/size.js ***!
  \*************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var baseKeys = __webpack_require__(/*! ./_baseKeys */ 34),
    getTag = __webpack_require__(/*! ./_getTag */ 38),
    isArrayLike = __webpack_require__(/*! ./isArrayLike */ 52),
    isString = __webpack_require__(/*! ./isString */ 54),
    stringSize = __webpack_require__(/*! ./_stringSize */ 57);

/** `Object#toString` result references. */
var mapTag = '[object Map]',
    setTag = '[object Set]';

/**
 * Gets the size of `collection` by returning its length for array-like
 * values or the number of own enumerable string keyed properties for objects.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object|string} collection The collection to inspect.
 * @returns {number} Returns the collection size.
 * @example
 *
 * _.size([1, 2, 3]);
 * // => 3
 *
 * _.size({ 'a': 1, 'b': 2 });
 * // => 2
 *
 * _.size('pebbles');
 * // => 7
 */
function size(collection) {
  if (collection == null) {
    return 0;
  }
  if (isArrayLike(collection)) {
    return isString(collection) ? stringSize(collection) : collection.length;
  }
  var tag = getTag(collection);
  if (tag == mapTag || tag == setTag) {
    return collection.size;
  }
  return baseKeys(collection).length;
}

module.exports = size;


/***/ }),
/* 34 */
/*!******************************************!*\
  !*** ./node_modules/lodash/_baseKeys.js ***!
  \******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var isPrototype = __webpack_require__(/*! ./_isPrototype */ 35),
    nativeKeys = __webpack_require__(/*! ./_nativeKeys */ 36);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeys;


/***/ }),
/* 35 */
/*!*********************************************!*\
  !*** ./node_modules/lodash/_isPrototype.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

module.exports = isPrototype;


/***/ }),
/* 36 */
/*!********************************************!*\
  !*** ./node_modules/lodash/_nativeKeys.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(/*! ./_overArg */ 37);

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

module.exports = nativeKeys;


/***/ }),
/* 37 */
/*!*****************************************!*\
  !*** ./node_modules/lodash/_overArg.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;


/***/ }),
/* 38 */
/*!****************************************!*\
  !*** ./node_modules/lodash/_getTag.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var DataView = __webpack_require__(/*! ./_DataView */ 39),
    Map = __webpack_require__(/*! ./_Map */ 48),
    Promise = __webpack_require__(/*! ./_Promise */ 49),
    Set = __webpack_require__(/*! ./_Set */ 50),
    WeakMap = __webpack_require__(/*! ./_WeakMap */ 51),
    baseGetTag = __webpack_require__(/*! ./_baseGetTag */ 9),
    toSource = __webpack_require__(/*! ./_toSource */ 19);

/** `Object#toString` result references. */
var mapTag = '[object Map]',
    objectTag = '[object Object]',
    promiseTag = '[object Promise]',
    setTag = '[object Set]',
    weakMapTag = '[object WeakMap]';

var dataViewTag = '[object DataView]';

/** Used to detect maps, sets, and weakmaps. */
var dataViewCtorString = toSource(DataView),
    mapCtorString = toSource(Map),
    promiseCtorString = toSource(Promise),
    setCtorString = toSource(Set),
    weakMapCtorString = toSource(WeakMap);

/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
var getTag = baseGetTag;

// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag) ||
    (Map && getTag(new Map) != mapTag) ||
    (Promise && getTag(Promise.resolve()) != promiseTag) ||
    (Set && getTag(new Set) != setTag) ||
    (WeakMap && getTag(new WeakMap) != weakMapTag)) {
  getTag = function(value) {
    var result = baseGetTag(value),
        Ctor = result == objectTag ? value.constructor : undefined,
        ctorString = Ctor ? toSource(Ctor) : '';

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString: return dataViewTag;
        case mapCtorString: return mapTag;
        case promiseCtorString: return promiseTag;
        case setCtorString: return setTag;
        case weakMapCtorString: return weakMapTag;
      }
    }
    return result;
  };
}

module.exports = getTag;


/***/ }),
/* 39 */
/*!******************************************!*\
  !*** ./node_modules/lodash/_DataView.js ***!
  \******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ 3),
    root = __webpack_require__(/*! ./_root */ 1);

/* Built-in method references that are verified to be native. */
var DataView = getNative(root, 'DataView');

module.exports = DataView;


/***/ }),
/* 40 */
/*!**********************************************!*\
  !*** ./node_modules/lodash/_baseIsNative.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ 16),
    isMasked = __webpack_require__(/*! ./_isMasked */ 45),
    isObject = __webpack_require__(/*! ./isObject */ 18),
    toSource = __webpack_require__(/*! ./_toSource */ 19);

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

module.exports = baseIsNative;


/***/ }),
/* 41 */
/*!********************************************!*\
  !*** ./node_modules/lodash/_freeGlobal.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(/*! ./../webpack/buildin/global.js */ 42)))

/***/ }),
/* 42 */
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 43 */
/*!*******************************************!*\
  !*** ./node_modules/lodash/_getRawTag.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(/*! ./_Symbol */ 17);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),
/* 44 */
/*!************************************************!*\
  !*** ./node_modules/lodash/_objectToString.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),
/* 45 */
/*!******************************************!*\
  !*** ./node_modules/lodash/_isMasked.js ***!
  \******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var coreJsData = __webpack_require__(/*! ./_coreJsData */ 46);

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

module.exports = isMasked;


/***/ }),
/* 46 */
/*!********************************************!*\
  !*** ./node_modules/lodash/_coreJsData.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ 1);

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

module.exports = coreJsData;


/***/ }),
/* 47 */
/*!******************************************!*\
  !*** ./node_modules/lodash/_getValue.js ***!
  \******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

module.exports = getValue;


/***/ }),
/* 48 */
/*!*************************************!*\
  !*** ./node_modules/lodash/_Map.js ***!
  \*************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ 3),
    root = __webpack_require__(/*! ./_root */ 1);

/* Built-in method references that are verified to be native. */
var Map = getNative(root, 'Map');

module.exports = Map;


/***/ }),
/* 49 */
/*!*****************************************!*\
  !*** ./node_modules/lodash/_Promise.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ 3),
    root = __webpack_require__(/*! ./_root */ 1);

/* Built-in method references that are verified to be native. */
var Promise = getNative(root, 'Promise');

module.exports = Promise;


/***/ }),
/* 50 */
/*!*************************************!*\
  !*** ./node_modules/lodash/_Set.js ***!
  \*************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ 3),
    root = __webpack_require__(/*! ./_root */ 1);

/* Built-in method references that are verified to be native. */
var Set = getNative(root, 'Set');

module.exports = Set;


/***/ }),
/* 51 */
/*!*****************************************!*\
  !*** ./node_modules/lodash/_WeakMap.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ 3),
    root = __webpack_require__(/*! ./_root */ 1);

/* Built-in method references that are verified to be native. */
var WeakMap = getNative(root, 'WeakMap');

module.exports = WeakMap;


/***/ }),
/* 52 */
/*!********************************************!*\
  !*** ./node_modules/lodash/isArrayLike.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ 16),
    isLength = __webpack_require__(/*! ./isLength */ 53);

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;


/***/ }),
/* 53 */
/*!*****************************************!*\
  !*** ./node_modules/lodash/isLength.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;


/***/ }),
/* 54 */
/*!*****************************************!*\
  !*** ./node_modules/lodash/isString.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ 9),
    isArray = __webpack_require__(/*! ./isArray */ 55),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ 56);

/** `Object#toString` result references. */
var stringTag = '[object String]';

/**
 * Checks if `value` is classified as a `String` primitive or object.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a string, else `false`.
 * @example
 *
 * _.isString('abc');
 * // => true
 *
 * _.isString(1);
 * // => false
 */
function isString(value) {
  return typeof value == 'string' ||
    (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag);
}

module.exports = isString;


/***/ }),
/* 55 */
/*!****************************************!*\
  !*** ./node_modules/lodash/isArray.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;


/***/ }),
/* 56 */
/*!*********************************************!*\
  !*** ./node_modules/lodash/isObjectLike.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),
/* 57 */
/*!********************************************!*\
  !*** ./node_modules/lodash/_stringSize.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var asciiSize = __webpack_require__(/*! ./_asciiSize */ 58),
    hasUnicode = __webpack_require__(/*! ./_hasUnicode */ 60),
    unicodeSize = __webpack_require__(/*! ./_unicodeSize */ 61);

/**
 * Gets the number of symbols in `string`.
 *
 * @private
 * @param {string} string The string to inspect.
 * @returns {number} Returns the string size.
 */
function stringSize(string) {
  return hasUnicode(string)
    ? unicodeSize(string)
    : asciiSize(string);
}

module.exports = stringSize;


/***/ }),
/* 58 */
/*!*******************************************!*\
  !*** ./node_modules/lodash/_asciiSize.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var baseProperty = __webpack_require__(/*! ./_baseProperty */ 59);

/**
 * Gets the size of an ASCII `string`.
 *
 * @private
 * @param {string} string The string inspect.
 * @returns {number} Returns the string size.
 */
var asciiSize = baseProperty('length');

module.exports = asciiSize;


/***/ }),
/* 59 */
/*!**********************************************!*\
  !*** ./node_modules/lodash/_baseProperty.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;


/***/ }),
/* 60 */
/*!********************************************!*\
  !*** ./node_modules/lodash/_hasUnicode.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/** Used to compose unicode character classes. */
var rsAstralRange = '\\ud800-\\udfff',
    rsComboMarksRange = '\\u0300-\\u036f',
    reComboHalfMarksRange = '\\ufe20-\\ufe2f',
    rsComboSymbolsRange = '\\u20d0-\\u20ff',
    rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
    rsVarRange = '\\ufe0e\\ufe0f';

/** Used to compose unicode capture groups. */
var rsZWJ = '\\u200d';

/** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
var reHasUnicode = RegExp('[' + rsZWJ + rsAstralRange  + rsComboRange + rsVarRange + ']');

/**
 * Checks if `string` contains Unicode symbols.
 *
 * @private
 * @param {string} string The string to inspect.
 * @returns {boolean} Returns `true` if a symbol is found, else `false`.
 */
function hasUnicode(string) {
  return reHasUnicode.test(string);
}

module.exports = hasUnicode;


/***/ }),
/* 61 */
/*!*********************************************!*\
  !*** ./node_modules/lodash/_unicodeSize.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/** Used to compose unicode character classes. */
var rsAstralRange = '\\ud800-\\udfff',
    rsComboMarksRange = '\\u0300-\\u036f',
    reComboHalfMarksRange = '\\ufe20-\\ufe2f',
    rsComboSymbolsRange = '\\u20d0-\\u20ff',
    rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
    rsVarRange = '\\ufe0e\\ufe0f';

/** Used to compose unicode capture groups. */
var rsAstral = '[' + rsAstralRange + ']',
    rsCombo = '[' + rsComboRange + ']',
    rsFitz = '\\ud83c[\\udffb-\\udfff]',
    rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
    rsNonAstral = '[^' + rsAstralRange + ']',
    rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
    rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
    rsZWJ = '\\u200d';

/** Used to compose unicode regexes. */
var reOptMod = rsModifier + '?',
    rsOptVar = '[' + rsVarRange + ']?',
    rsOptJoin = '(?:' + rsZWJ + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
    rsSeq = rsOptVar + reOptMod + rsOptJoin,
    rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';

/** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
var reUnicode = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');

/**
 * Gets the size of a Unicode `string`.
 *
 * @private
 * @param {string} string The string inspect.
 * @returns {number} Returns the string size.
 */
function unicodeSize(string) {
  var result = reUnicode.lastIndex = 0;
  while (reUnicode.test(string)) {
    ++result;
  }
  return result;
}

module.exports = unicodeSize;


/***/ }),
/* 62 */
/*!*********************************************!*\
  !*** ./app/algorithms/graphs/bellmanFor.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = bellmanFor;
/**
 * Created by ginollerena on 6/29/18.
 */

function bellmanFor(nodes, edges, s) {
  var distances = {};
  var parents = {};
  nodes.forEach(function (node) {
    distances[node] = Infinity;
    parents[node] = null;
  });
  distances[s] = 0;

  for (var iteration = 0; iteration < nodes.length - 1; iteration += 1) {
    edges.forEach(function (edge) {
      var _edge = _slicedToArray(edge, 3),
          from = _edge[0],
          to = _edge[1],
          weight = _edge[2];

      if (distances[from] + weight < distances[to]) {
        distances[to] = distances[from] + weight;
        parents[to] = from;
      }
    });
  };

  edges.forEach(function (edge) {
    var _edge2 = _slicedToArray(edge, 3),
        from = _edge2[0],
        to = _edge2[1],
        weight = _edge2[2];

    if (distances[from] + weight < distances[to]) {
      return undefined;
    }
  });

  return {
    distances: distances,
    parents: parents
  };
}

/***/ }),
/* 63 */
/*!*************************************************!*\
  !*** ./app/algorithms/graphs/floyd-warshall.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = floyd_warshall;
/**
 * Created by ginollerena on 6/29/18.
 */

function floyd_warshall(dist, P, n) {
  //  including array P to record paths

  //  initialise P to all -1s
  for (var k = 0; k < n; ++k) {
    for (var i = 0; i < n; ++i) {
      for (var j = 0; j < n; ++j) {
        //  check for path from i to k and k to j
        if (dist[i][k] * dist[k][j] !== 0 && i !== j) {
          //  check shorter path from i to k and k to j
          if (dist[i][k] + dist[k][j] < dist[i][j] || dist[i][j] === 0) {
            dist[i][j] = dist[i][k] + dist[k][j];
            P[i][j] = k + 1;
          }
        }
      }
    }
  }
}

/***/ }),
/* 64 */
/*!*******************************************!*\
  !*** ./app/algorithms/graphs/dijkstra.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = dijkstra;
/**
 * Created by ginollerena on 6/30/18.
 */

var ENABLE_LOG = false;

function log(text) {
  if (ENABLE_LOG) console.log(text);
}

function lowestCostNode() {
  var costs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var processed = arguments[1];

  var keys = Reflect.ownKeys(costs);
  return keys.reduce(function (lowest, node) {
    if (lowest === null || costs[node] < costs[lowest]) {
      if (!processed.includes(node)) {
        lowest = node;
      }
    }
    return lowest;
  }, null);
};

// function that returns the minimum cost and path to reach Finish
function dijkstra(graph, startNodeName, endNodeName) {

  // track the lowest cost to reach each node
  var costs = {};
  costs[endNodeName] = "Infinity";
  costs = Object.assign(costs, graph[startNodeName]);

  // track paths
  var parents = { endNodeName: null };
  for (var child in graph[startNodeName]) {
    parents[child] = startNodeName;
  }

  // track nodes that have already been processed
  var processed = [];

  var node = lowestCostNode(costs, processed);

  while (node) {
    var cost = costs[node];
    var children = graph[node];
    for (var n in children) {
      if (String(n) === String(startNodeName)) {
        log("WE DON'T GO BACK TO START");
      } else {
        log("StartNodeName: " + startNodeName);
        log("Evaluating cost to node " + n + " (looking from node " + node + ")");
        log("Last Cost: " + costs[n]);
        var newCost = cost + children[n];
        log("New Cost: " + newCost);
        if (!costs[n] || costs[n] > newCost) {
          costs[n] = newCost;
          parents[n] = node;
          log("Updated cost und parents");
        } else {
          log("A shorter path already exists");
        }
      }
    }
    log('node', node);
    processed.push(node);
    node = lowestCostNode(costs, processed);
  }

  var optimalPath = [endNodeName];
  var parent = parents[endNodeName];
  while (parent) {
    optimalPath.push(parent);
    parent = parents[parent];
  }
  optimalPath.reverse();

  var results = {
    distance: costs[endNodeName],
    path: optimalPath
  };

  return results;
};

/***/ }),
/* 65 */,
/* 66 */
/*!**************************************!*\
  !*** ./app/structures/BinaryTree.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by ginollerena on 6/30/18.
 */

var BinaryTree = function () {
  function BinaryTree() {
    _classCallCheck(this, BinaryTree);

    this.root = {};
    this.branch = [];
  }

  _createClass(BinaryTree, [{
    key: "createNode",
    value: function createNode(value) {
      return { value: value, left: {}, right: {} };
    }
  }, {
    key: "_insertNode",
    value: function _insertNode(node, value) {
      if (!node.value) {
        var newNode = this.createNode(value);
        node = Object.assign(node, newNode);
      } else if (node.value > value) {
        this._insertNode(node.left, value);
      } else {
        this._insertNode(node.right, value);
      }
    }
  }, {
    key: "_height",
    value: function _height(node) {
      /* base case tree is empty */
      if (!node.value) {
        return 0;
      }
      /* If tree is not empty then height = 1 + max of left height and right heights */
      return 1 + Math.max(this.height(node.left), this.height(node.right));
    }
  }, {
    key: "_diameter",
    value: function _diameter(node) {
      /* base case where tree is empty */
      if (!node.value) return 0;

      /* get the height of left and right sub-trees */
      var lheight = this._height(node.left);
      var rheight = this._height(node.right);

      /* get the diameter of left and right sub-trees */
      var ldiameter = this._diameter(node.left);
      var rdiameter = this._diameter(node.right);

      /* Return max of following three
       1) Diameter of left subtree
       2) Diameter of right subtree
       3) Height of left subtree + height of right subtree + 1 */
      return Math.max(lheight + rheight + 1, Math.max(ldiameter, rdiameter));
    }
  }, {
    key: "_preOrder",
    value: function _preOrder(node) {
      if (!node.value) return;

      console.log(node.value);

      this._preOrder(node.left);
      this._preOrder(node.right);
    }
  }, {
    key: "_getBranchUtil",
    value: function _getBranchUtil(node, branch, fncConditionUtil) {

      if (!node.value) return branch;

      var myBranchL = this._getBranchUtil(node.left, [].concat(_toConsumableArray(branch)).concat([node.value]), fncConditionUtil);
      var myBranchR = this._getBranchUtil(node.right, [].concat(_toConsumableArray(branch)).concat([node.value]), fncConditionUtil);

      var sumA = myBranchL && myBranchL.length > 0 ? fncConditionUtil(myBranchL) : 0;
      var sumB = myBranchR && myBranchR.length > 0 ? fncConditionUtil(myBranchR) : 0;

      if (sumA > sumB) return myBranchL;else return myBranchR;
    }
  }, {
    key: "getBranch",
    value: function getBranch(fnc) {
      if (!fnc) {
        fnc = function fnc(l) {
          return l.reduce(function (acc, n) {
            acc += n;return acc;
          }, 0);
        };
      }
      return this._getBranchUtil(this.root, this.branch, fnc);
    }
  }, {
    key: "getPreOrder",
    value: function getPreOrder() {
      this._preOrder(this.root);
    }
  }, {
    key: "getHeight",
    value: function getHeight() {
      return this.height(this.root);
    }
  }, {
    key: "getDiameter",
    value: function getDiameter() {
      return this._diameter(this.root);
    }
  }, {
    key: "doNodeInsertion",
    value: function doNodeInsertion(value) {
      this._insertNode(this.root, value);
    }
  }]);

  return BinaryTree;
}();

exports.default = BinaryTree;

/***/ })
],[20]);
//# sourceMappingURL=bundle.js.map